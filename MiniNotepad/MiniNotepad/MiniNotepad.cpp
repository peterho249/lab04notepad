// MiniNotepad.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "MiniNotepad.h"
#include <windowsx.h>
#include <WinUser.h>
#include <commdlg.h>
#include <iostream>
#include <fstream>
using namespace std;
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND contentTextbox;							// The typing space handle
OPENFILENAME ofn;								// Open file structure
const int BUFFER_SIZE = 260;					// Size of file name buffer
bool newFileFlag = TRUE;						// Flag of new file creation
bool fileChangeFlag = FALSE;					// Flag of chance the content of file
bool errorFlag = FALSE;							// Flag of error occur
char* contentOfFile = nullptr;
ifstream inFile;
ofstream outFile;
WCHAR szFile[BUFFER_SIZE];



// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hwnd);
void OnClose(HWND hWnd);
void OnSize(HWND hWnd, UINT state, int cx, int cy);
void OpenFile(char* content, OPENFILENAME &ofn);
void SaveFile(char* content, OPENFILENAME &ofn);
bool IsChange(HWND hWnd, char* content);
int QuitAndSaveFile(HWND hWnd);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MININOTEPAD, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MININOTEPAD));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MININOTEPAD));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_BTNFACE+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MININOTEPAD);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_SIZE, OnSize);
		HANDLE_MSG(hWnd, WM_CLOSE, OnClose);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	// Get system font
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	HFONT hFont = CreateFont(lf.lfHeight, lf.lfWidth,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);

	contentTextbox = CreateWindowW(L"edit", L"", 
		WS_CHILD | WS_VISIBLE | ES_MULTILINE | WS_HSCROLL | WS_VSCROLL | WS_MAXIMIZE | ES_LEFT,
		0, 0, 0, 0, hWnd, nullptr, hInst, nullptr);
	SendMessage(contentTextbox, WM_SETFONT, (WPARAM)hFont, TRUE);
	SetFocus(contentTextbox);
	return TRUE;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	switch (id)
	{
	case ID_FILE_OPEN:
	{
		// Initialize OPENFILENAME
		ZeroMemory(&ofn, sizeof(ofn));
		ofn.lStructSize = sizeof(ofn);
		ofn.hwndOwner = hWnd;
		ofn.lpstrFile = szFile;
		ofn.nMaxFile = BUFFER_SIZE;
		ofn.lpstrFilter = L"Text document (*.txt) \0*.txt\0All files (*.*) \0*.*\0";
		ofn.nFilterIndex = 1;
		ofn.lpstrFileTitle = NULL;
		ofn.lpstrInitialDir = NULL;
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

		if (GetOpenFileName(&ofn))
		{
			OpenFile(contentOfFile, ofn);
			newFileFlag = FALSE;
		}
	}
		break;
	case ID_FILE_SAVE:
	{
		if (newFileFlag)
		{
			// Initialize OPENFILENAME
			ZeroMemory(&ofn, sizeof(ofn));
			ofn.lStructSize = sizeof(ofn);
			ofn.hwndOwner = hWnd;
			ofn.nMaxFile = BUFFER_SIZE;
			ofn.lpstrFile = szFile;
			ofn.lpstrDefExt = L"txt";
			ofn.lpstrFilter = L"Text document (*.txt) \0*.txt\0All files (*.*) \0*.*\0";
			ofn.nFilterIndex = 1;
			ofn.lpstrFileTitle = NULL;
			ofn.lpstrInitialDir = NULL;
			ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

			if (GetSaveFileName(&ofn))
			{
				SaveFile(contentOfFile, ofn);
				fileChangeFlag = FALSE;
			}
		}

		SaveFile(contentOfFile, ofn);
		fileChangeFlag = FALSE;
	}
		break;
	case ID_EDIT_CUT:
		SendMessage(contentTextbox, WM_CUT, 0, 0);
		break;
	case ID_EDIT_COPY:
		SendMessage(contentTextbox, WM_COPY, 0, 0);
		break;
	case ID_EDIT_PASTE:
		SendMessage(contentTextbox, WM_PASTE, 0, 0);
		break;
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
	{
		if (QuitAndSaveFile(hWnd))
			DestroyWindow(hWnd);
	}
		break;
	}
}


void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	// TODO: Add any drawing code that uses hdc here...
	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hWnd)
{
	PostQuitMessage(0);
}

void OnSize(HWND hWnd, UINT state, int cx, int cy)
{
	MoveWindow(contentTextbox, 0, 0, cx, cy, TRUE);
}

void OpenFile(char* content, OPENFILENAME &ofn)
{
	inFile.open(ofn.lpstrFile, ios::binary);
	inFile.seekg(0, ios::end);
	size_t fileSize = inFile.tellg();
	inFile.seekg(0, ios::beg);

	contentOfFile = new char[fileSize + 1];
	inFile.read(contentOfFile,fileSize);
	contentOfFile[fileSize] = 0;
	inFile.close();

	size_t byteConverted;
	WCHAR* wContent = new WCHAR[fileSize + 1];
	mbstowcs_s(&byteConverted, wContent, fileSize + 1,contentOfFile, fileSize + 1);
	SetWindowText(contentTextbox, wContent);
}

void SaveFile(char* content, OPENFILENAME &ofn)
{
	outFile.open(ofn.lpstrFile, ios::binary);

	size_t byteConverted;
	size_t fileSize = GetWindowTextLength(contentTextbox);
	WCHAR* wContent = new WCHAR[fileSize + 1];
	if (contentOfFile != nullptr)
		delete[] contentOfFile;
	contentOfFile = new char[fileSize + 1];
	GetWindowText(contentTextbox, wContent, fileSize + 1);
	wcstombs_s(&byteConverted, contentOfFile, fileSize + 1, wContent, fileSize + 1);

	outFile.write(contentOfFile, fileSize);
	outFile.close();
}

bool IsChange(HWND hWnd, char* sourceContent)
{
	size_t byteConverted;
	size_t size = GetWindowTextLength(contentTextbox);
	WCHAR* wContent = new WCHAR[size + 1];
	char* content = new char[size + 1];
	GetWindowText(contentTextbox, wContent, size + 1);
	wcstombs_s(&byteConverted, content, size + 1, wContent, size + 1);

	if (sourceContent == nullptr)
	{
		if (strlen(content) != 0)
			return TRUE;
		else
			return FALSE;
	}

	if (strlen(sourceContent) != strlen(content))
		return TRUE;
	else
	{
		for (int i = 0; i < size; i++)
		{
			if (sourceContent[i] != content[i])
				return TRUE;
		}
	}

	return FALSE;
}

int QuitAndSaveFile(HWND hWnd)
{
	if (IsChange(contentTextbox,contentOfFile))
	{
		int idButton = MessageBox(hWnd, L"Do you want to save!", L"Warning", MB_YESNOCANCEL);
		if (idButton == IDCANCEL)
			return 0;

		if (idButton == IDYES)
		{
			if (newFileFlag)
			{
				// Initialize OPENFILENAME
				ZeroMemory(&ofn, sizeof(ofn));
				ofn.lStructSize = sizeof(ofn);
				ofn.hwndOwner = hWnd;
				ofn.nMaxFile = BUFFER_SIZE;
				ofn.lpstrFile = szFile;
				ofn.lpstrDefExt = L"txt";
				ofn.lpstrFilter = L"Text document (*.txt) \0*.txt\0All files (*.*) \0*.*\0";
				ofn.nFilterIndex = 1;
				ofn.lpstrFileTitle = NULL;
				ofn.lpstrInitialDir = NULL;
				ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

				if (GetSaveFileName(&ofn))
					SaveFile(contentOfFile, ofn);
			}

			SaveFile(contentOfFile, ofn);
		}
	}
	return 1;
}

void OnClose(HWND hWnd)
{
	if (QuitAndSaveFile(hWnd))
		DestroyWindow(hWnd);
}