# README #

## Information ##
* ID: 1512077
* Name: Ho Xuan Dung

## Configuration System ##
* OS: Windows 10 x64
* IDE: Visual Studio 2017

## Function ##
* Typing content into a edit control.
* Open a file.
* Save a file.
* Can cut - copy - paste from menu.

### Note ###
* The application can just work with ascii text file.
* You can open and save file with dialog.

## Application Flow ##
### Main Flow ###
* Click File > Open to open a text file, fill the edit control with content, click File > Save to save your working.

### Addiction Flow ###
* If you want to cut, selecte the text and click Edit > Cut.
* If you want to copy, selecte the text and click Edit > Copy.
* If you want to paste content from clipboard, click Edit > Paste.
* When you exit the app but not save file yet, a message box will notify you. If you want to save, click Yes, else click No.
* If there is no chance of the content and you exit the app, there is nothing appear.

## BitBucket Link ##
* https://bitbucket.org/peterho249/lab04notepad